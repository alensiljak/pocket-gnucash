# Pocket GnuCash

Pocket companion to GnuCash

Client-server web app (PWA) that should be able to run on a mobile device.

The server component is to be implemented in Python.

The client component is to be a Progressive Web Application implemented with Vue.js and Bootstrap.

The app is supposed to be used to create transactions on a mobile device and sync to the GnuCash SQLite database.

## Development

`pip install -r requirements.txt`
