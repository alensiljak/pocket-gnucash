"""
Template for the server app
"""
from flask import Blueprint, Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def index():
    """ The default route. Homepage. """
    return render_template('index.html')

@app.route("/shutdown")
def shutdown():
    """ Shutdown the server """
    shutdown_server()
    return 'Server shutting down...'

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


if __name__ == '__main__':
    app.run()
